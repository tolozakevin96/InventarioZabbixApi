# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 16:13:59 2022

@author: ktoloza
"""
import mysql.connector
from datetime import datetime
import ParametrosInventario as parametros
import QueryInventario as query


def insertar_mysql(query):
    try:
        conn = mysql.connector.connect(**parametros.mysql_args)
        cur = conn.cursor()
        cur.execute(query)
        cur.close()
        conn.close()
    except mysql.connector.Error as err:
        print(datetime.now())
        print("Error: ", err)
        print("Sentencia insert: ", query)
        return None
    else:
        return None

def constructor(data,parametro):
    values = ''
    try:
        for row in data:
            values += str(row)+','
        if parametro == 'hosts':
            return query.INSERT_ZABBIX_HOST.format(datos=values[:-1])
        if parametro == 'interface':
            return query.INSERT_ZABBIX_INTERFACE.format(datos=values[:-1])
        if parametro == 'item':
            return query.INSERT_ZABBIX_ITEM.format(datos=values[:-1])
        if parametro == 'hostgroup':
            return query.INSERT_ZABBIX_HOSTGROUP.format(datos=values[:-1])
    except Exception as error:
        print("Error sentencia sql;", error)    
    

def get_interface(hostid):
    interfacezabbix = parametros.zapi.do_request('hostinterface.get',
        {                                  
        "output": ["hostid","ip","port"],
        "hostids": hostid
    })['result']
    
    return interfacezabbix

def get_item(hostid):
    itemzabbix = parametros.zapi.do_request('item.get',
          {      
        "output": ["hostid","itemid","lastvalue"],
        "hostids": hostid,
        "search": {
            "name": "informacion del sistema"
            },
        })['result']

    return itemzabbix


def get_hostgroup(hostid):
    hostgroupzabbix = parametros.zapi.do_request('host.get',
            {
        "output": ["hostids"],
        "selectGroups": [
            "groupid",
            "name",
        ],
        "hostids":hostid
    }
        )['result']
    
    return hostgroupzabbix


if __name__ == '__main__':
    print("Connected to Zabbix API Version %s" % parametros.zapi.api_version())
    lista_interface_formateada = []
    lista_item_formateada = []
    lista_host_formateada= []
    lista_hostgroup_formateada= []
    lista_host_activo = []
               
    for lista in query.templatezabbix:
        listahosts = lista['hosts'] #json con solo la informacion de los host sin tomar en cuenta el nombre del template
        #Recorro para obtener la informacion ordenada de los hosts
        for inventario in listahosts:
            listahostselement = []
            listahostselement.append(int(inventario['hostid']))
            listahostselement.append(inventario['name'])
            listahostselement.append(inventario['host'])
            listahostselement.append(int(inventario['status']))
            listahostselement.append(lista['name'])
            lista_host_formateada.append(tuple(listahostselement))
            #Almaceno todos los host que traigo de las plantilla Linux, Win, Aix
            lista_host_activo.append(int(inventario['hostid']))
    
    #Obtengo del query get.item y get.inferface enviandole como parametro la lista de host que obtengo
    interfacehost = get_interface(lista_host_activo)
    itemhost = get_item(lista_host_activo)
    hostgroup = get_hostgroup(lista_host_activo)
    
    #Recorro para obtener la informacion ordena de los groups 
    for listahostgroup in hostgroup:
        listagroups = listahostgroup['groups']
        for inventariogroups in listagroups:
            listagroupelement = []
            listagroupelement.append(int(listahostgroup['hostid']))
            listagroupelement.append(int(inventariogroups['groupid']))
            listagroupelement.append(inventariogroups['name'])
            lista_hostgroup_formateada.append(tuple(listagroupelement))
    
    #Recorro para obtener la informacion ordena de las interfaces
    for inventario_interface in interfacehost:
        listainterelement = []
        listainterelement.append(int(inventario_interface['hostid']))
        listainterelement.append(int(inventario_interface['interfaceid']))
        listainterelement.append(inventario_interface['ip'])
        listainterelement.append(inventario_interface['port'])
        lista_interface_formateada.append(tuple(listainterelement))
    
    #Recorro para obtener la informacion ordena de los items
    for inventario_item in itemhost:
        listaitemelement = []
        listaitemelement.append(int(inventario_item['itemid']))
        listaitemelement.append(int(inventario_item['hostid']))
        listaitemelement.append(inventario_item['lastvalue'])
        lista_item_formateada.append(tuple(listaitemelement))        
        
    
    #Construyo el string para insertar las listas formateadas a su tabla respectiva
    lista_hosts = constructor(lista_host_formateada,"hosts")
    insertar_mysql(lista_hosts)  
    lista_interface = constructor(lista_interface_formateada,"interface")
    insertar_mysql(lista_interface)    
    lista_items = constructor(lista_item_formateada,"item")
    insertar_mysql(lista_items)
    host_group_contructor = constructor(lista_hostgroup_formateada, 'hostgroup')        
    insertar_mysql(host_group_contructor)
   
  
        
            
        