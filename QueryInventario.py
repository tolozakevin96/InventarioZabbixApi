# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 14:25:48 2022

@author: ktoloza
"""
import ParametrosInventario as parametros


#obtengo todos los host vinculados al templateid 
templatezabbix = parametros.zapi.do_request('template.get',
        {
        "output": ["templateid","name"],
        "selectHosts": [
            "hostid",
            "name",
            "status",
            "host"
        ],
        "templateids": ["10257","10255","10260","10078"],
    }
        )['result']


INSERT_ZABBIX_HOST = """
INSERT INTO INV_ZABBIX_HOST(IDHOST,HOSTNAME,HOST,STATUS,TEMPLATE) 
VALUES {datos}
"""

INSERT_ZABBIX_INTERFACE = """
INSERT INTO INV_ZABBIX_INTERFACE(IDHOST,INTERFACEID,IP,PORT) 
VALUES {datos}
"""

INSERT_ZABBIX_ITEM = """
INSERT INTO INV_ZABBIX_ITEM(ITEMID,IDHOST,DESCRIPTION) 
VALUES {datos}
"""
INSERT_ZABBIX_HOSTGROUP = """
INSERT INTO INV_ZABBIX_HOSTGROUP(IDHOST,IDHOSTGROUP,GROUPNAME) 
VALUES {datos}
"""

